#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::inc1() {
    if(ui->inc1->value() > 0)   {
        QString output = QTime::currentTime().toString("hh:mm:ss:")+" HP:     "+ui->val1->text()+" -> ";
        ui->val1->setValue(ui->val1->value()+ui->inc1->value());
        output += ui->val1->text()+"\n";
        ui->log->setPlainText(output+ui->log->toPlainText());
    }
}
void MainWindow::inc2() {
    if(ui->inc2->value() > 0)   {
        QString output = QTime::currentTime().toString("hh:mm:ss:")+" Atk:    "+ui->val2->text()+" -> ";
        ui->val2->setValue(ui->val2->value()+ui->inc2->value());
        output += ui->val2->text()+"\n";
        ui->log->setPlainText(output+ui->log->toPlainText());
    }
}
void MainWindow::inc3() {
    if(ui->inc3->value() > 0)   {
        QString output = QTime::currentTime().toString("hh:mm:ss:")+" Def:    "+ui->val3->text()+" -> ";
        ui->val3->setValue(ui->val3->value()+ui->inc3->value());
        output += ui->val3->text()+"\n";
        ui->log->setPlainText(output+ui->log->toPlainText());
    }
}
void MainWindow::inc4() {
    if(ui->inc4->value() > 0)   {
        QString output = QTime::currentTime().toString("hh:mm:ss:")+" SpAtk:  "+ui->val4->text()+" -> ";
        ui->val4->setValue(ui->val4->value()+ui->inc4->value());
        output += ui->val4->text()+"\n";
        ui->log->setPlainText(output+ui->log->toPlainText());
    }
}
void MainWindow::inc5() {
    if(ui->inc5->value() > 0)   {
        QString output = QTime::currentTime().toString("hh:mm:ss:")+" SpDef:  "+ui->val5->text()+" -> ";
        ui->val5->setValue(ui->val5->value()+ui->inc5->value());
        output += ui->val5->text()+"\n";
        ui->log->setPlainText(output+ui->log->toPlainText());
    }
}
void MainWindow::inc6() {
    if(ui->inc6->value() > 0)   {
        QString output = QTime::currentTime().toString("hh:mm:ss:")+" Spd:    "+ui->val6->text()+" -> ";
        ui->val6->setValue(ui->val6->value()+ui->inc6->value());
        output += ui->val6->text()+"\n";
        ui->log->setPlainText(output+ui->log->toPlainText());
    }
}
void MainWindow::incAll()   {
    MainWindow::inc1();
    MainWindow::inc2();
    MainWindow::inc3();
    MainWindow::inc4();
    MainWindow::inc5();
    MainWindow::inc6();
}
