/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Sat Apr 3 02:28:06 2010
**      by: Qt User Interface Compiler version 4.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QPlainTextEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpinBox>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLineEdit *name;
    QLabel *label;
    QGroupBox *groupBox;
    QPlainTextEdit *log;
    QGroupBox *groupBox_2;
    QLabel *label_6;
    QPushButton *btn4;
    QLabel *label_8;
    QSpinBox *val4;
    QLabel *label_2;
    QPushButton *btn1;
    QLabel *label_7;
    QSpinBox *val6;
    QLabel *label_9;
    QSpinBox *val5;
    QLabel *label_10;
    QLabel *label_5;
    QSpinBox *inc2;
    QSpinBox *val2;
    QPushButton *btn2;
    QPushButton *btn3;
    QSpinBox *inc4;
    QPushButton *btn6;
    QSpinBox *inc1;
    QPushButton *btn5;
    QSpinBox *val3;
    QSpinBox *inc3;
    QSpinBox *inc5;
    QSpinBox *inc6;
    QSpinBox *val1;
    QLabel *label_3;
    QLabel *label_4;
    QPushButton *btnAll;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(434, 232);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        name = new QLineEdit(centralWidget);
        name->setObjectName(QString::fromUtf8("name"));
        name->setGeometry(QRect(50, 10, 131, 20));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 10, 31, 21));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(190, 0, 231, 221));
        log = new QPlainTextEdit(groupBox);
        log->setObjectName(QString::fromUtf8("log"));
        log->setGeometry(QRect(10, 20, 211, 191));
        QFont font;
        font.setFamily(QString::fromUtf8("Courier"));
        log->setFont(font);
        log->setReadOnly(true);
        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(10, 40, 171, 181));
        label_6 = new QLabel(groupBox_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(10, 110, 46, 21));
        btn4 = new QPushButton(groupBox_2);
        btn4->setObjectName(QString::fromUtf8("btn4"));
        btn4->setGeometry(QRect(90, 90, 31, 23));
        label_8 = new QLabel(groupBox_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(10, 10, 31, 16));
        val4 = new QSpinBox(groupBox_2);
        val4->setObjectName(QString::fromUtf8("val4"));
        val4->setGeometry(QRect(120, 90, 41, 22));
        val4->setMaximum(256);
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 30, 46, 21));
        btn1 = new QPushButton(groupBox_2);
        btn1->setObjectName(QString::fromUtf8("btn1"));
        btn1->setGeometry(QRect(90, 30, 31, 23));
        label_7 = new QLabel(groupBox_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(10, 130, 46, 20));
        val6 = new QSpinBox(groupBox_2);
        val6->setObjectName(QString::fromUtf8("val6"));
        val6->setGeometry(QRect(120, 130, 41, 22));
        val6->setMaximum(256);
        label_9 = new QLabel(groupBox_2);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(120, 10, 31, 16));
        val5 = new QSpinBox(groupBox_2);
        val5->setObjectName(QString::fromUtf8("val5"));
        val5->setGeometry(QRect(120, 110, 41, 22));
        val5->setMaximum(256);
        label_10 = new QLabel(groupBox_2);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(50, 10, 51, 16));
        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(10, 90, 46, 21));
        inc2 = new QSpinBox(groupBox_2);
        inc2->setObjectName(QString::fromUtf8("inc2"));
        inc2->setGeometry(QRect(50, 50, 41, 22));
        val2 = new QSpinBox(groupBox_2);
        val2->setObjectName(QString::fromUtf8("val2"));
        val2->setGeometry(QRect(120, 50, 41, 22));
        val2->setMaximum(256);
        btn2 = new QPushButton(groupBox_2);
        btn2->setObjectName(QString::fromUtf8("btn2"));
        btn2->setGeometry(QRect(90, 50, 31, 23));
        btn3 = new QPushButton(groupBox_2);
        btn3->setObjectName(QString::fromUtf8("btn3"));
        btn3->setGeometry(QRect(90, 70, 31, 23));
        inc4 = new QSpinBox(groupBox_2);
        inc4->setObjectName(QString::fromUtf8("inc4"));
        inc4->setGeometry(QRect(50, 90, 41, 22));
        btn6 = new QPushButton(groupBox_2);
        btn6->setObjectName(QString::fromUtf8("btn6"));
        btn6->setGeometry(QRect(90, 130, 31, 23));
        inc1 = new QSpinBox(groupBox_2);
        inc1->setObjectName(QString::fromUtf8("inc1"));
        inc1->setGeometry(QRect(50, 30, 41, 22));
        btn5 = new QPushButton(groupBox_2);
        btn5->setObjectName(QString::fromUtf8("btn5"));
        btn5->setGeometry(QRect(90, 110, 31, 23));
        val3 = new QSpinBox(groupBox_2);
        val3->setObjectName(QString::fromUtf8("val3"));
        val3->setGeometry(QRect(120, 70, 41, 22));
        val3->setMaximum(256);
        inc3 = new QSpinBox(groupBox_2);
        inc3->setObjectName(QString::fromUtf8("inc3"));
        inc3->setGeometry(QRect(50, 70, 41, 22));
        inc5 = new QSpinBox(groupBox_2);
        inc5->setObjectName(QString::fromUtf8("inc5"));
        inc5->setGeometry(QRect(50, 110, 41, 22));
        inc6 = new QSpinBox(groupBox_2);
        inc6->setObjectName(QString::fromUtf8("inc6"));
        inc6->setGeometry(QRect(50, 130, 41, 22));
        val1 = new QSpinBox(groupBox_2);
        val1->setObjectName(QString::fromUtf8("val1"));
        val1->setGeometry(QRect(120, 30, 41, 22));
        val1->setMaximum(256);
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(10, 50, 46, 21));
        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 70, 46, 21));
        btnAll = new QPushButton(groupBox_2);
        btnAll->setObjectName(QString::fromUtf8("btnAll"));
        btnAll->setGeometry(QRect(80, 150, 51, 21));
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);
        QObject::connect(btn1, SIGNAL(clicked()), MainWindow, SLOT(inc1()));
        QObject::connect(btn2, SIGNAL(clicked()), MainWindow, SLOT(inc2()));
        QObject::connect(btn3, SIGNAL(clicked()), MainWindow, SLOT(inc3()));
        QObject::connect(btn4, SIGNAL(clicked()), MainWindow, SLOT(inc4()));
        QObject::connect(btn5, SIGNAL(clicked()), MainWindow, SLOT(inc5()));
        QObject::connect(btn6, SIGNAL(clicked()), MainWindow, SLOT(inc6()));
        QObject::connect(btnAll, SIGNAL(clicked()), MainWindow, SLOT(incAll()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Flib's EV Tracker", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "Name", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("MainWindow", "Change log", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QString());
        label_6->setText(QApplication::translate("MainWindow", "SpDef", 0, QApplication::UnicodeUTF8));
        btn4->setText(QApplication::translate("MainWindow", "->", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("MainWindow", "Stat", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "HP", 0, QApplication::UnicodeUTF8));
        btn1->setText(QApplication::translate("MainWindow", "->", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("MainWindow", "Spd", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("MainWindow", "Value", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("MainWindow", "Increment", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("MainWindow", "SpAtk", 0, QApplication::UnicodeUTF8));
        btn2->setText(QApplication::translate("MainWindow", "->", 0, QApplication::UnicodeUTF8));
        btn3->setText(QApplication::translate("MainWindow", "->", 0, QApplication::UnicodeUTF8));
        btn6->setText(QApplication::translate("MainWindow", "->", 0, QApplication::UnicodeUTF8));
        btn5->setText(QApplication::translate("MainWindow", "->", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MainWindow", "Atk", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MainWindow", "Def", 0, QApplication::UnicodeUTF8));
        btnAll->setText(QApplication::translate("MainWindow", "--All->", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
